# Berthin_JPhysChemLett_14_101_2023

Contains input files used to perform the simulations of the article:

Nanostructural Organization in a Biredox Ionic Liquid

Roxanne Berthin, Alessandra Serva, Olivier Fontaine and Mathieu Salanne
*J. Phys. Chem. Lett.*, 14, 101-106  2022

https://pubs.acs.org/doi/full/10.1021/acs.jpclett.2c03330 ([see here for a preprint](https://doi.org/10.26434/chemrxiv-2022-pgjgv))

Each folder contains 2 input files for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls). The corresponding systems are the following:

* BIL_TEMPOmIM-AQTFSI: biredox ionic liquid made of anthraquinone-functionalized TFSI anions and TEMPO-functionalized imidazolium cations 

* IL_BMIM-TFSI: conventional ionic liquid made of TFSI anions and 1-butyl-3-methylimidazolium cations
